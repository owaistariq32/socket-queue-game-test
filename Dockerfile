FROM node:14-slim

RUN mkdir /usr/src/app

WORKDIR /usr/src/app

COPY package.json package.json

RUN yarn

RUN yarn global add nodemon

COPY . .

EXPOSE 3000

CMD [ "yarn", "start" ]