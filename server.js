const io = require("socket.io-client");
// let url = "https://streamv1.ludolush.com"
let url = "https://streamv2-dev.ludolush.com"

// let url = "http://192.168.10.49:4010"
// let url = "http://localhost:4002"

let createTokenCounter = 0;
let gameCounter = 0
let tokenCreatedCount = 0
let accessToken = '';
const range = {min: 1500, max: 3000}, 
delta = range.max - range.min;
var rand = Math.round(range.min + Math.random() * delta);
// console.log(rand)

setInterval(()=>{
    if(createTokenCounter<100) {
        startTest();
        createTokenCounter++;
    }
    console.log(`createTokenCounter: ${createTokenCounter}, tokenCreatedCount:${tokenCreatedCount}, gameCounter:${gameCounter}`);
}, 333);

function startTest() {
    let socket = io(url, { transports: ['websocket'] });
    console.log("createTokenCounter",createTokenCounter);
    socket.on('connect', function() {
        createToken(socket);

        socket.on("tokenCreated", (tokenInfo) => {
            clearInterval(socket.createTokenInterval);
            socket.userid = tokenInfo.id;
            tokenCreatedCount++;
            socket.accessToken = tokenInfo.accessToken;
            console.log("tokenCreated",tokenCreatedCount,socket.userid,socket.id);
            if(tokenCreatedCount<50)
                authToken(socket)
            
        });

        socket.on("tokenAuthenticated", (tokenInfo) => {
            clearInterval(socket.authTokenInterval);
            console.log("tokenAuthenticated",tokenInfo,socket.userid,socket.id);
            socket.notInGame = tokenInfo.notInGame
            // if(tokenCreatedCount<50)
            //     joinPublicBoard(socket);

        });

        socket.on("rulesSent", (rulesSent) => {

            console.log('rulesSent',rulesSent,socket.userid,socket.id);
        });
        

        socket.on("diceRolled", (diceRolledInfo) => {
            clearInterval(socket.rollDiceInterval);
            console.log('diceRolled',socket.userid,socket.id);

        });


        socket.on("pawnMoved", (board) => {
            console.log('pawnMoved',socket.userid,socket.id,socket.boardId);
            
        });

        socket.on("turnPassed", (board) => {
            console.log('turnPassed',socket.userid,socket.id,socket.boardId);
            updateAutoPlay(socket);
            rollDice(socket);
            
        });

        socket.on("autoPlayUpdated", (board) => {

            console.log('autoPlayUpdated',socket.userid,socket.id,socket.boardId);
            clearInterval(socket.updateAutoPlayInterval);
            
        });
        
        socket.on("notInGame", (board) => {

            console.log('notInGame',socket.userid,socket.id);
            clearInterval(socket.boardStateLiteInterval);
            
        });

        socket.on("boardStateLiteSent", (board) => {

            console.log('boardStateLiteSent',socket.userid,socket.id,socket.boardId);
            clearInterval(socket.boardStateLiteInterval);
            
        });


        socket.on("minimalProfileData", (minimalProfileDataInfo) => {
            console.log("minimalProfileData",minimalProfileDataInfo);
        });

        socket.on("boardJoined", (boardJoinedInfo) => {
            console.log("boardJoined",socket.userid,socket.id);
            socket.locationId = boardJoinedInfo.locationId;
            clearInterval(socket.joinBoardInterval);

        });

        

        socket.on("gameStarted", (board) => {
            gameCounter++;
            socket.boardId = board.boardId;
            console.log('game started',gameCounter,socket.userid,socket.id);
            rollDice(socket);
            setInterval(()=>{
                boardStateLite(socket);
                sendMessage(socket);
                sendGift(socket);
            }, 5000);

            
        });

        socket.on('disconnect', function() {
            socketReconnect(socket);
        })

        socket.on("gameEnded", (board) => {
            console.log('gameEnded',socket.userid,socket.id)
        });

    });
}

function socketReconnect(socket) {
    if(socket )
        socket.connect();

}

function sendGift(socket) {
    if(socket.id!=undefined) {
        let receiverLocationId = 0;
        let senderLocationId = socket.locationId;
        if(senderLocationId==0)
            receiverLocationId = 2;
        socket.emit("sendGift",{senderLocationId,receiverLocationId,giftIndex:2})
    }
}

function createToken(socket) {
    if(socket.id!=undefined) {
        let deviceId = "awais"+Date.now()+Math.floor(Math.random() * 30000);
        console.log("deviceId creating Token",deviceId,socket.id);
        socket.emit("createToken", {deviceId:deviceId});
        socket.deviceId = deviceId;
        // clearInterval(socket.createTokenInterval);
        // socket.createTokenInterval = setInterval(function() { createToken(socket); }, 5000);
    }
}

function sendMessage(socket) {
    if(socket.id!=undefined) {
        socket.emit('sendMessage',{message:"stress Testing"});
    }
}

function boardStateLite(socket) {
    if(socket.id!=undefined) {
        socket.emit('sendBoardStateLite',{});
        clearInterval(socket.boardStateLiteInterval);
        socket.boardStateLiteInterval = setInterval(function() { boardStateLite(socket); }, 5000);
    }

}

function authToken(socket) {
    if(socket.id!=undefined) {
        console.log('auth ing Token',socket.id);
        socket.emit("authToken", {accessToken:socket.accessToken});
        // clearInterval(socket.authTokenInterval);
        // socket.authTokenInterval = setInterval(function() { authToken(socket); }, 5000);
    }

}

function joinPublicBoard(socket) {

    if(socket.id!=undefined) {
        let modeTypes = ['classic','arrow','quick'];
        let modeType = modeTypes.sort(() => 0.5 - Math.random())[0];
        socket.emit("joinPublicBoard", {playerType:2,modeType,betAmount:500});
        // clearInterval(socket.joinBoardInterval);
        // socket.joinBoardInterval = setInterval(function() { joinPublicBoard(socket); }, 5000);
    }

}

function sendProfile(socket) {
    if(socket.id!=undefined) {
        socket.emit("sendProfile", {id:socket.userid});
    }

}

function sendRules(socket) {
    if(socket.id!=undefined) {
        socket.emit("sendRules", {});
    }
}

function userHeaderInformation(socket) {
    if(socket.id!=undefined) {
        socket.emit("userHeaderInformation", {});
    }
}

function userHeaderInformation(socket) {
    if(socket.id!=undefined) {
        socket.emit("sendDoubleSignupReward", {"sendProfile":true,"hasVideoWatched":true});
    }
}

function rollDice(socket) {
    if(socket.id!=undefined) {
        const rndInt = Math.floor(Math.random() * 6) + 1
        socket.emit('rollDice',{number:rndInt,undoCount:0});
        clearInterval(socket.rollDiceInterval);
        // socket.rollDiceInterval = setInterval(function() { rollDice(socket); }, 5000);
        // console.log('rollDice',rndInt,socket.userid,socket.id);
    }
}

function updateAutoPlay(socket) {
    if(socket.id!=undefined) {
        socket.emit('updateAutoPlay',{});
        // clearInterval(socket.updateAutoPlayInterval);
        // socket.updateAutoPlayInterval = setInterval(function() { updateAutoPlay(socket); }, 5000);
    }
}